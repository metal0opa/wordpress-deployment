# Define provider and region
provider "aws" {
  region = var.region
}

# Create EC2 instance
resource "aws_instance" "wordpress" {
  instance_type = var.instance_type
  ami           = "ami-06ac5f5ed93f43a1d"
  key_name      = "mykey"
  vpc_security_group_ids = [
    aws_security_group.wordpress.id,
  ]

  user_data = <<-EOF
              #!/bin/bash
              sudo yum update -y
              sudo amazon-linux-extras install docker -y
              sudo service docker start
              sudo docker run -d --name wordpress -p 80:80 wordpress
              EOF

  tags = {
    Name = "wordpress-instance"
  }

}

# Create RDS instance
resource "aws_db_instance" "wordpressdb" {
  allocated_storage    = 20
  engine               = "mysql"
  instance_class       = var.db_instance_type
  db_name              = "wordpressdb"
  username             = "admin"
  password             = var.db_password
  parameter_group_name = "default.mysql8.0"
  vpc_security_group_ids = [
    aws_security_group.wordpressdb.id,
  ]
}

# Create security group for EC2-Internet communication
resource "aws_security_group" "wordpress" {
  name_prefix = "wordpress"
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Create security group for RDS- EC2 communication
resource "aws_security_group" "wordpressdb" {
  name_prefix = "wordpressdb"
  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = [" ${aws_instance.wordpress.private_ip}/32"]
  }
}

# Create Elastic IP
resource "aws_eip" "wordpress" {
  instance = aws_instance.wordpress.id
}

# Associate Elastic IP with EC2 instance
resource "aws_eip_association" "wordpress" {
  instance_id   = aws_instance.wordpress.id
  allocation_id = aws_eip.wordpress.id
}

# Bonus Points: Create ACM certificate
resource "aws_acm_certificate" "wordpress" {
  domain_name       = var.domain_name
  validation_method = "DNS"
  tags = {
    Name = "wordpress-certificate"
  }

  lifecycle {
    create_before_destroy = true
  }
}

# Bonus Points: Create Route53 record
resource "aws_route53_record" "wordpress" {
  zone_id = var.zone_id
  name    = var.domain_name
  type    = "A"
  alias {
    zone_id                = var.zone_id
    name                   = var.domain_name
    evaluate_target_health = false
  }

  lifecycle {
    create_before_destroy = true
  }
}

# Bonus Points: Deploy Let's Encrypt
resource "null_resource" "letsencrypt" {
  provisioner "local-exec" {
    command = "certbot certonly --standalone --non-interactive --agree-tos --email admin@example.com -d example.com -d www.example.com"
  }

  depends_on = [
    aws_route53_record.wordpress,
  ]
}

# Bonus Points: Install phpMyAdmin
resource "null_resource" "phpmyadmin" {
  provisioner "remote-exec" {
    inline = [
      "sudo amazon-linux-extras install epel -y",
      "sudo yum install -y httpd php php-mysql php-gd php-mbstring php-zip php-json",
      "sudo systemctl start httpd.service",
      "sudo systemctl enable httpd.service",
      "sudo yum install -y https://dl.fedoraproject.org/pub/epel/epel-release",
      "sudo yum install -y phpmyadmin",
      "sudo sed -i '/Require ip ::1/c\\Require all granted' /etc/httpd/conf.d/phpMyAdmin.conf",
      "sudo sed -i '/Allow from ::1/c\\Allow from all' /etc/httpd/conf.d/phpMyAdmin.conf",
      "sudo systemctl restart httpd.service"
    ]
    connection {
      type        = "ssh"
      user        = aws_db_instance.wordpressdb.username
      private_key = file(var.private_key_path)
      host        = aws_instance.wordpress.public_ip
    }

  }
}
