variable "aws_access_key" {}
variable "aws_secret_key" {}
variable "region" {}
variable "private_key_path" {}
variable "db_password" {
  description = "The password for the WordPress database"
}
variable "instance_type" {
  default     = "t4g.small"
  description = "The type of EC2 instance to use for the WordPress application"
}
variable "db_instance_type" {
  default     = "db.t4g.micro"
  description = "The type of RDS instance to use for the WordPress database"
}
variable "domain_name" {
  description = "The domain name to use for the WordPress application"
}
variable "zone_id" {
  description = "The ID of the Route 53 hosted zone to use for the WordPress application"
}
