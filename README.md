To run project, you can follow these steps:

Install Terraform on your local machine. You can find the installation instructions for your operating system on the official Terraform website: https://www.terraform.io/downloads.html

1. git clone the repository

2. Run the terraform plan command to check your Terraform script and ensure that everything is correct.

If there are no errors, run the terraform apply command to deploy the WordPress application.

3. To test the script, you can access the WordPress application using the Elastic IP address that is assigned to the EC2 instance. You can also test the phpMyAdmin installation by accessing it via the EC2 instance's public IP address and the URL http://<ec2-public-ip>/phpmyadmin.

4. To roll back and delete all resources created by the Terraform script, you can run the terraform destroy command.

5. To test the Let's Encrypt and domain name bonus feature, you can use a tool like nslookup or dig to verify that the domain name is pointing to the Elastic IP address that was assigned to the EC2 instance.

6. To test the phpMyAdmin installation, you can access the phpMyAdmin web interface by navigating to http://<EC2_instance_public_IP_address>/phpmyadmin in a web browser. Log in with the username and password you specified in the Terraform script.

Note: Before running the terraform destroy command, make sure that you have backed up any data that you need to keep, as this command will delete all resources created by the Terraform script.
